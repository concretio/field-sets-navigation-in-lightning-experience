/**
* @Author Chirag kochar
* @description test class for the controller FieldSetNavigationController which will be used to callout the mock webservices(Metadata api) for the field set manipulation
*/
@IsTest
public class FieldSetNavigationControllerTest {
    public class WebServiceMockImpl implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{	
			if(request instanceof MetadataService.retrieve_element)
				response.put('response_x', new MetadataService.retrieveResponse_element());
			else if(request instanceof MetadataService.checkDeployStatus_element)
				response.put('response_x', new MetadataService.checkDeployStatusResponse_element());
			else if(request instanceof MetadataService.listMetadata_element)
				response.put('response_x', new MetadataService.listMetadataResponse_element());
			else if(request instanceof MetadataService.checkRetrieveStatus_element)
				response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			else if(request instanceof MetadataService.describeMetadata_element)
				response.put('response_x', new MetadataService.describeMetadataResponse_element());
			else if(request instanceof MetadataService.deploy_element)
				response.put('response_x', new MetadataService.deployResponse_element());
            else if(request instanceof MetadataService.updateMetadata_element){
                MetadataService.SaveResult result = new MetadataService.SaveResult();
                result.success = true;
                MetadataService.updateMetadataResponse_element responseElement = new MetadataService.updateMetadataResponse_element();
                responseElement.result = new MetadataService.SaveResult[] {result};
                response.put('response_x', responseElement);
            }
            else if(request instanceof MetadataService.renameMetadata_element)
                response.put('response_x', new MetadataService.renameMetadataResponse_element());
            else if(request instanceof  MetadataService.cancelDeploy_element)
                response.put('response_x', new MetadataService.cancelDeployResponse_element());
            else if(request instanceof  MetadataService.deleteMetadata_element)
                response.put('response_x', new MetadataService.deleteMetadataResponse_element());
            else if(request instanceof  MetadataService.upsertMetadata_element)
                response.put('response_x', new MetadataService.upsertMetadataResponse_element());
            else if(request instanceof  MetadataService.createMetadata_element){
                MetadataService.SaveResult result = new MetadataService.SaveResult();
                result.success = true;
                MetadataService.createMetadataResponse_element responseElement = new MetadataService.createMetadataResponse_element();
                responseElement.result = new MetadataService.SaveResult[] {result};
                response.put('response_x', responseElement);}
            else if(request instanceof  MetadataService.deployRecentValidation_element)
                response.put('response_x', new MetadataService.deployRecentValidationResponse_element());
            else if(request instanceof MetadataService.describeValueType_element)
                response.put('response_x', new MetadataService.describeValueTypeResponse_element());
            else if(request instanceof MetadataService.checkRetrieveStatus_element)
                response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			return;
		}
	}
    
    public class HttpCalloutMockImpl implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        String json = '{"encoding" : "UTF-8","maxBatchSize" : 200,"sobjects" : [ {"layoutable" : false,"name" : "Account", "triggerable" : false, "custom" : true}, {"layoutable" : true,"name" : "AcceptedEventRelation","triggerable" : true, "custom" : true}]}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(json);
        res.setStatusCode(200);
        return res;
    }
}
    /**	
	* @name getSobjectList
	* @description used to get the object list 
	*/
    @isTest static void getSobjectList()
    {
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMockImpl());
        List<String> objList = FieldSetNavigationController.getSObjectList();
        test.stopTest();
        System.assertEquals(true, objList.size() > 0);  
    }
    
    
    /**	
	* @name createFieldSetTest
	* @description used to create the field set 
	*/
    @isTest static void createFieldSetTest()
    {
        String objName = 'Account';
        String fsName = 'Account_Field_Set';
        String fsLabel = 'Account Field Set';
        String description = 'Testing purpose';
        List<String> fsList = new List<String>();
        fsList.add('name');
        fsList.add('site');
        test.startTest();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        FieldSetNavigationController.getAllFields(objName);
        FieldSetNavigationController.getFieldSetNames(objName);
        String response = FieldSetNavigationController.createFieldSet(objName, fsName, fsLabel, description, fsList);
        test.stopTest();
        System.assertEquals('success', response);
    }
    /**	
	* @name createFieldSetTest
	* @description used to update the field set 
	*/
    @isTest static void updateFieldSetTest()
    {
        String objName = 'Account';
        String fsName = 'Account_Field_Set';
        String fsLabel = 'Account Field Set';
        String description = 'Testing purpose';
        List<String> fsList = new List<String>();
        fsList.add('name');
        fsList.add('site');
        fsList.add('phone');
        test.startTest();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        FieldSetNavigationController.getAllFields(objName);
        FieldSetNavigationController.getFieldsOfTheSelectedFieldSet(objName,fsName);
        string response = FieldSetNavigationController.updateFieldSet(objName, fsName, fsLabel, description, fsList);
        test.stopTest();
        System.assertEquals('success', response);
    }
    /**	
	* @name createFieldSetTest
	* @description used to delete the field set 
	*/
    @isTest static void deleteFieldSetTest()
    {
        String objName = 'Account';
        String fsName = 'Account_Field_Set';
        String fsLabel = 'Account Field Set';
        String description = 'Testing purpose';
        List<String> fsList = new List<String>();
        fsList.add('name');
        fsList.add('site');
        fsList.add('phone');
        test.startTest();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        FieldSetNavigationController.getAllFields(objName);
        FieldSetNavigationController.getFieldsOfTheSelectedFieldSet(objName,fsName);
        string response = FieldSetNavigationController.deleteFieldSet(objName, fsName);
        test.stopTest();
        System.assertEquals('success', response);
    }
}