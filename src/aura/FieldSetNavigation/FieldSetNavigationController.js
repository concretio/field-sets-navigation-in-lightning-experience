({
    // get all the object list 
	doInit : function(component, event, helper) {
		var action = component.get("c.getSObjectList");
		action.setCallback(this, function(response)
        {
            var state = response.getState();
            if (state === "SUCCESS") {
            	var res = response.getReturnValue();
            	var objectList = response.getReturnValue();
                var opts = [];
				opts.push({
			        label: "--- None ---",
			        value: ""
			    });
			    for(var i = 0; i < objectList.length ; i++)
            	{
            		opts.push({
		            label : objectList[i],
		            value : objectList[i]
		        	});
            	}
            	component.set("v.options", opts);
			}
    }),
        // Send action off to be executed
        $A.enqueueAction(action);
	},
	getSelectedObject : function(component, event, helper) {
        helper.getSelectedObjectFieldSets(component,event);
	},
    //handle menu select edit and delete
    handleMenuSelect : function(component, event, helper) {
        var getFieldSetName = event.getSource().get("v.value");
        var getAction = event.getParam("value");
        component.set("v.apiName",getFieldSetName);
        component.set("v.fieldSetName",getFieldSetName);
        if(getAction == "Edit"){
            var cmpTarget = component.find('FieldModalbox');
            var cmpBack = component.find('ModalBackdrop');
            $A.util.addClass(cmpTarget, 'slds-fade-in-open');
            $A.util.addClass(cmpBack, 'slds-backdrop--open');  
            component.set("v.listOptions", " ");
            helper.getFieldsOfTheSelectedFieldSet(component, getFieldSetName);
        }
        else if(getAction == "Delete"){
            var cmpTarget = component.find('DeleteModalBox');
            var cmpBack = component.find('ModalBackdrop');
            $A.util.addClass(cmpTarget, 'slds-fade-in-open');
            $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
        }
    },
    deleteSelectedFieldSet : function(component, event, helper){
        var fieldSetName = component.get("v.fieldSetName");
        helper.handleDeleteFieldSet(component, fieldSetName);
    },
    cancelNewModal : function(component,event,helper){   
        component.set("v.flag", false);
		helper.closeModalBox(component,event);
   	},
    cancelEditModal : function(component,event,helper){    
        helper.cancelModalBox(component,event);
   	},
    cancelDeleteModal : function(component,event,helper){    
        helper.cancelDeleteBox(component,event);
   	},
    openmodal:function(component,event,helper) {
        helper.openModalBox(component,event);
	},
    openEditFieldModal : function(component,event,helper) {
        var label = component.find("label").get("v.value");
        var name = component.find("apiName").get("v.value");
        var desc = component.find("desc").get("v.value");
        if(label && name && desc)
        {
            component.set("v.flag", true);
            helper.getAllFields(component,event);
            helper.openEditFieldModalBox(component,event);
        }
        else
        {
			return false;    
        }
	},
	//used to set the api name of the field set
    setApiName : function(component,event,helper) {
        helper.apiName(component,event);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    handleChange : function (component, event, helper) {
        // Get the list of the "value" attribute on all the selected options
        helper.onChange(component, event);
    },
    saveFieldList : function (component, event, helper){
        helper.handleSaveFieldList(component, event);
    }
})