 App Description:  A generic lightning app, which Can manage(create/edit/delete) a Fieldset for any sobjects in lightning experience
                   .This application can be inbuilt in any organisation and easy to use and the user interface are designed by using the 
                   Salesforce Lightning Design System(SLDS) and compatible for any device.
 
 API Services used: Salesforce "Metadata(SOAP) Api" for creating,updating and deleting the field sets.
                    Salesforce "REST Api" for fetching objects that are available for cration of field set
                    (basically those objects on which we can create custom fields are available for field set manipulation) 
 
 Application component used: Component named "FieldSetNavigation" It consist of UI for showing the list of object and 
                             popup for creating ,editing and deleting the field set of selected objects.
                             For Styling the ui Salesforce lightning design system(SLDS) has been used.
                             Apex classes- 
                               *'MetadataService' class for the web service callout ,
                               *'FieldSetNavigationController' for fetching the list of object and pre existing list of field set 
                                corresponding to particular object .handling the create,update and delete request.
                               *SobjectListModel class is a wrapper class for value(custom,layoutable,triggerable) which is coming from the 
                                rest callout response.
                             Apex vf page-
                               *'getSeesionId' vf page is used for getting the seesion id as we can't get the session id in lightning aura 
                                enabled methods.
 
 Demo Video URL: https://drive.google.com/open?id=1DuSASJ3Kptk-g_XI6wanwKYhKulesoL8